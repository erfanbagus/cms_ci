<?php
/* Create By Manh Quyen Ta */
class Injector {
	public $js = array();
	public $css = array();
	function __construct() {
		$this->injectBower();
		$this->injectjs('apps');
	}
	public function js($compress = false) {
		foreach ($this->js as $name => $url) {
			echo '<script id="' . $name . '" type="text/javascript" src="' . $url . '"></script>';
		}
	}
	public function css() {
		foreach ($this->css as $name => $url) {
			echo '<link id="' . $name . '" rel="stylesheet" type="text/css" href="' . $url . '" />';
		}
	}
	private function min_file() {

	}
	private function injectBower() {
		$file = FCPATH . 'bower.json';
		$bower = file_get_contents($file);
		$bower_array = json_decode($bower);
		$dependencies = $bower_array->dependencies;
		foreach ($dependencies as $dir => $version) {
			$this->BowerDependencies($dir,$version);
			$bowerLibs = FCPATH . 'bower_components/' . $dir . '/' . 'bower.json';
			$bowerLibs = json_decode(file_get_contents($bowerLibs));
			if(!empty($bowerLibs->dependencies)){
				foreach ($bowerLibs->dependencies as $dir => $version) {
					$this->BowerDependencies($dir,$version);
				}
			}
		}
	}
	private function BowerDependencies($dir,$version)
	{
		$bowerLibs = FCPATH . 'bower_components/' . $dir . '/' . 'bower.json';
		$bowerLibs = json_decode(file_get_contents($bowerLibs));
		if (is_array($bowerLibs->main)) {
			foreach ($bowerLibs->main as $lib) {
				$extension = $this->getExtension('bower_components/' . $dir . '/' . $lib);
				$url = base_url() . 'bower_components/' . $dir . '/' . str_replace('./', '', $lib . '?ver=' . str_replace('~', '', $version));
				$this->enquee($bowerLibs->name, $url, $extension);
			}
		} else {
			$extension = $this->getExtension('bower_components/' . $dir . '/' . $bowerLibs->main);
			$this->enquee($bowerLibs->name, base_url() . 'bower_components/' . $dir . '/' . str_replace('./', '', $bowerLibs->main . '?ver=' . str_replace('~', '', $version)), $extension);
		}
	}
	private function injectCss() {

	}
	public function injectjs($dir) {
		$fileOndir = $this->read_all_files($dir,'js');
		$i = 0;
		foreach($fileOndir as $jsapp){
			$this->enquee_js('app-'.$i,str_replace("\\","/",base_url().$jsapp));
			$i++;
		}
	}
	private function read_all_files($root = '.',$ex){
		$files  = array();
		$directories  = array();
		$last_letter  = $root[strlen($root)-1];
		$root  = ($last_letter == '\\' || $last_letter == '/') ? $root : $root.DIRECTORY_SEPARATOR;
		$directories[]  = $root;
		while (sizeof($directories)) {
			$dir  = array_pop($directories);
			if ($handle = opendir($dir)) {
				while (false !== ($file = readdir($handle))) {
					if ($file == '.' || $file == '..') {
						continue;
					}
					$file  = $dir.$file;
					if (is_dir($file)) {
						$directory_path = $file.DIRECTORY_SEPARATOR;
						array_push($directories, $directory_path);
					} elseif (is_file($file)) {
						if($this->getExtension($file) === "js")
							$files[]  = $file;
					}
				}
				closedir($handle);
			}
		}
		return $files;
	}
	function enquee($name, $file, $ex) {
		if ($ex === 'js') {
			$this->enquee_js($name, $file);
		} else {
			$this->enquee_css($name, $file);
		}
	}
	function enquee_js($name, $url) {
		if (!array_key_exists($name, $this->js)) {
			$this->js[$name] = $url;
		}
	}
	function enquee_css($name, $url) {
		if (!array_key_exists($name, $this->css)) {
			$this->css[$name] = $url;
		}
	}
	private function getExtension($file) {
		$file_read = pathinfo($file);
		return $file_read['extension'];
	}
}
