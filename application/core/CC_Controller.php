<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Class auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 * @property  Blade_render blade_render
 * @property  Arc_nestable arc_nestable
 * @property  M_get m_get
 */
class CC_Controller extends MX_Controller  {

	function __construct()
	{
		parent::__construct();
		$this->load->library(array('ion_auth'));
		$this->load->helper(array('language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->load->model('m_get');
		$this->lang->load('auth');
	}

	public function C_login($username,$password,$remember)
	{
			return $this->ion_auth->login($username, $password,  (bool) $remember);
	}

	public function C_cek_login($link_redirect_true=null,$link_redirect_false=null){
		if (!$this->ion_auth->logged_in())
		{
			if(!$link_redirect_false==null)
			redirect($link_redirect_false, 'refresh');
		}else{
			if(!$link_redirect_true==null)
			redirect($link_redirect_true, 'refresh');
		}
	}

	public function C_logout($link_redirect){

		$logout = $this->ion_auth->logout();
		if($logout){
		  redirect($link_redirect, 'refresh');
		}
	}

	public function getdata(){
		$data=array();
		$data['title_apps']="My Dashboard";
		$data['uri_segment']=$this->uri->uri_string;
		$data['breadcrumb']=explode('/',$data['uri_segment']);
		$data['csrf_token']['name']=$this->security->get_csrf_token_name();
		$data['csrf_token']['value']=$this->security->get_csrf_hash();
		return $data;
	}


}
