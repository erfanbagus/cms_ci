<?php
/**
 * Created by PhpStorm.
 * User: Hatori-Harazawa
 * Date: 22/11/2018
 * Time: 23:14
 */

if (! function_exists('env')) {

	function env($name, $default = null) {

		if ($value = getenv($name)) {

			return $value;
		}

		return $default;
	}
}

if(!function_exists('my_test')){
	/**
	 * fungsi debug data return
	 * @param array $data
	 */
	function my_test($data=array())
	{
		echo "<pre>";
		var_dump($data);
		exit();
	}
}

if(!function_exists('myjson')){
	/**
	 * fungsi test json
	 * @param array $data
	 */
	function myjson($data=array()){
		header('Content-Type: application/json');
		echo json_encode($data);
		exit();
	}
}

if(! function_exists('assets')){
	/**
	 * @param string $src
	 * @param null $folder
	 * @return string
	 */
	function assets($src='',$folder=null) {
		if($folder){
			return base_url('assets/'.$folder.'/'.$src);
		}else{
			return base_url('assets/'.$src);
		}
	}
}

if(! function_exists('active_link')){
	function active_link($url_active,$link_menu) {

		if($url_active === $link_menu){
			return 'active open';
		}else{
			return '';
		}

//		foreach ($ulr_active as $key =>$value){
//			var_dump($value);
//			if($value == $link_menu[$key]){
//				return 'active open';
//			}else{
//				return '';
//			}
//		}
//		if($ulr_active == $link_menu) {
//			return 'active';
//		}else{
//			return '';
//		}
	}
}


if(! function_exists('get_csrf_name')){
	function get_csrf_name() {
		return $this->security->get_csrf_token_name();
	}
}

if ( ! function_exists('active_anchor'))
{
	function active_anchor($url = NULL, $title = NULL, $key = NULL, $params = array())
	{
		if ($url && $key)
		{
			if($key == $url)
			{
				if (array_key_exists ('class' , $params))
				{
					$params['class'] .= ' active';
				}
				else
				{
					$params['class'] = 'active';
				}
			}
		}
		return anchor($url, $title, $params);
	}
}

