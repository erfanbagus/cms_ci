<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="description" content="bootstrap admin template">
	<meta name="author" content="">

	<title>{{$title_apps}}</title>


	{{--icon--}}
	<link rel="apple-touch-icon" href="{{assets('apple-touch-icon.png','images')}}">
	<link rel="shortcut icon" href="{{assets('favicon.ico','images')}}">
	{{--icon--}}

	<!-- Stylesheets -->
	<link rel="stylesheet" href="{{assets('bootstrap.min.css','vendor/remark/global/css')}}">
	<link rel="stylesheet" href="{{assets('bootstrap-extend.min.css','vendor/remark/global/css')}}">
	<link rel="stylesheet" href="{{assets('site.css','vendor/remark/assets/css')}}">
	<!-- Stylesheets -->

	<!-- Plugins -->

	<link rel="stylesheet" href="{{assets('animsition.css','vendor/remark/global/vendor/animsition')}}">
	<link rel="stylesheet" href="{{assets('asScrollable.css','vendor/remark/global/vendor/asscrollable')}}">
	<link rel="stylesheet" href="{{assets('switchery.css','vendor/remark/global/vendor/switchery')}}">
	<link rel="stylesheet" href="{{assets('introjs.css','vendor/remark/global/vendor/intro-js')}}">
	<link rel="stylesheet" href="{{assets('slidePanel.css','vendor/remark/global/vendor/slidepanel')}}">
	<link rel="stylesheet" href="{{assets('flag-icon.css','vendor/remark/global/vendor/flag-icon-css')}}">
	<link rel="stylesheet" href="{{assets('login-v3.css','vendor/remark/assets/examples/css/pages')}}">
	<!-- Plugins -->


	{{--font--}}
	<link rel="stylesheet" href="{{assets('font-awesome.css','vendor/remark/global/fonts/font-awesome')}}">
	<link rel="stylesheet" href="{{assets('web-icons.css','vendor/remark/global/fonts/web-icons')}}">
	<link rel="stylesheet" href="{{assets('brand-icons.css','vendor/remark/global/fonts/brand-icons')}}">
	<link rel='stylesheet' href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic">
	{{--font--}}

	{{--IE9--}}
	<script src="{{assets('html5shiv.min.js','vendor/remark/global/vendor/html5shiv')}}"></script>
	{{--IE9--}}

	{{--IE10--}}
	<script src="{{assets('media.match.min.js','vendor/remark/global/vendor/media-match')}}"></script>
	<script src="{{assets('respond.min.js','vendor/remark/global/vendor/respond')}}"></script>
	{{--IE10--}}

	<!-- Scripts -->
	<script src="{{assets('breakpoints.js','vendor/remark/global/vendor/breakpoints')}}"></script>
	<script>
		Breakpoints();
	</script>
	<!-- Scripts -->
	<style>
		body{

			zoom: 0.95;
			-moz-transform: scale(0.95);
		}
		.form-control {
			color: #2a577f;
			font-weight: 400;
		}
	</style>
</head>
<body class="animsition page-login-v3 layout-full">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->


<!-- Page -->
<div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">>
@yield('content')
</div>
<!-- End Page -->


<!-- Core  -->
<script src="{{assets('babel-external-helpers.js','vendor/remark/global/vendor/babel-external-helpers')}}"></script>
<script src="{{assets('jquery.js','vendor/remark/global/vendor/jquery')}}"></script>
<script src="{{assets('popper.min.js','vendor/remark/global/vendor/popper-js/umd')}}"></script>
<script src="{{assets('bootstrap.js','vendor/remark/global/vendor/bootstrap')}}"></script>
<script src="{{assets('animsition.js','vendor/remark/global/vendor/animsition')}}"></script>
<script src="{{assets('jquery.mousewheel.js','vendor/remark/global/vendor/mousewheel')}}"></script>
<script src="{{assets('jquery-asScrollbar.js','vendor/remark/global/vendor/asscrollbar')}}"></script>
<script src="{{assets('jquery-asScrollable.js','vendor/remark/global/vendor/asscrollable')}}"></script>
<script src="{{assets('jquery-asHoverScroll.js','vendor/remark/global/vendor/ashoverscroll')}}"></script>
<!-- Core  -->


<!-- Plugins -->
<script src="{{assets('switchery.js','vendor/remark/global/vendor/switchery')}}"></script>
<script src="{{assets('intro.js','vendor/remark/global/vendor/intro-js')}}"></script>
<script src="{{assets('screenfull.js','vendor/remark/global/vendor/screenfull')}}"></script>
<script src="{{assets('jquery-slidePanel.js','vendor/remark/global/vendor/slidepanel')}}"></script>
<script src="{{assets('jquery.placeholder.js','vendor/remark/global/vendor/jquery-placeholder')}}"></script>
<!-- Plugins -->


<!-- Scripts -->
<script src="{{assets('Component.js','vendor/remark/global/js')}}"></script>
<script src="{{assets('Plugin.js','vendor/remark/global/js')}}"></script>
<script src="{{assets('Base.js','vendor/remark/global/js')}}"></script>
<script src="{{assets('Config.js','vendor/remark/global/js')}}"></script>

<script src="{{assets('Menubar.js','vendor/remark/assets/js/Section')}}"></script>
<script src="{{assets('GridMenu.js','vendor/remark/assets/js/Section')}}"></script>
<script src="{{assets('Sidebar.js','vendor/remark/assets/js/Section')}}"></script>
<script src="{{assets('PageAside.js','vendor/remark/assets/js/Section')}}"></script>
<script src="{{assets('menu.js','vendor/remark/assets/js/Plugin')}}"></script>

<script src="{{assets('colors.js','vendor/remark/global/js/config')}}"></script>
<script src="{{assets('tour.js','vendor/remark/assets/js/config')}}"></script>
<script>Config.set('assets', '{{assets('','vendor/remark')}}');</script>
<!-- Scripts -->

<!-- Page -->
<script src="{{assets('Site.js','vendor/remark/assets/js')}}"></script>
<script src="{{assets('asscrollable.js','vendor/remark/global/js/Plugin')}}"></script>
<script src="{{assets('slidepanel.js','vendor/remark/global/js/Plugin')}}"></script>
<script src="{{assets('switchery.js','vendor/remark/global/js/Plugin')}}"></script>
<script src="{{assets('ecommerce.js','vendor/remark/assets/examples/js/dashboard')}}"></script>
<!-- Page -->

<script>
	(function(document, window, $){
		'use strict';

		var Site = window.Site;
		$(document).ready(function(){
			Site.run();
		});
	})(document, window, jQuery);
</script>
@yield('script')
</body>
</html>
