<div class="site-menubar">
	<div class="site-menubar-body">
		<div>
			<div>
				<ul class="site-menu" data-plugin="menu">
					<li class="site-menu-category">MENU</li>

					<li class="site-menu-item {{active_link($breadcrumb,['cms'])}}">
						<a class="animsition-link" href="{{base_url('cms')}}">
							<i class="site-menu-icon wb-grid-4" aria-hidden="true"></i>
							<span class="site-menu-title">Dashboard</span>
						</a>
					</li>
					<li class="site-menu-item has-sub {{active_link($breadcrumb,['cms','setting','menu'] )}}">
						<a href="javascript:void(0)">
							<i class="site-menu-icon wb-briefcase" aria-hidden="true"></i>
							<span class="site-menu-title">Settings</span>
							<span class="site-menu-arrow"></span>
						</a>
						<ul class="site-menu-sub">
							<li class="site-menu-item {{active_link($breadcrumb,['cms','setting','menu'] )}}">
								<a class="animsition-link" href="{{base_url('cms/setting/menu')}}">
									<span class="site-menu-title">Menu</span>
								</a>
							</li>
							<li class="site-menu-item">
								<a class="animsition-link" href="data_diskon.html">
									<span class="site-menu-title">Web</span>
								</a>
							</li>

						</ul>
					</li>

				</ul>
			</div>
		</div>
	</div>

</div>
