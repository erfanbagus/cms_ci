@extends('cms.layouts.app')

@section('head-body')
	<link rel="stylesheet" href="{{assets('nestable.min.css','vendor/remark/global/vendor/nestable')}}">
@endsection

@section('content')
	<!-- Modal -->
	<div class="modal fade" id="data_menu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Data Menu</h4>
				</div>
				<form method="post" id="input-data_menu"  onsubmit="return false" @submit.prevent="add_menu">
					<div class="modal-body">
						<div class="form-group">
							<input v-model="data_form.name" type="text" class="form-control" placeholder="Menu name" autofocus required  oninvalid="this.setCustomValidity('cannot be empty')" oninput="this.setCustomValidity('')">
						</div>
						<div class="form-group">
							<textarea v-model="data_form.description" type="text" class="form-control" placeholder="Menu description" style="height: 150px">
							</textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary">Save changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="border-bottom: 1px solid rgba(128,128,128,0.35)">
					Confirm Delete Data
				</div>

				<div class="modal-body">
					You are sure to delete this data [ @{{  name_hapus }} ]  ..?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<a @click="delete_menu()" class="btn btn-danger text-white " id="btn-ok">Delete</a>
				</div>
			</div>
		</div>
	</div>
	<!-- Panel Table Add Row -->
	<div class="container" id="panel">
		<div class="row">
			<div class="col-md-4 bg-white">
				<h4>Editor Menu</h4>
				<form onsubmit="return false" method="post">
					<div class="form-group form-material">
						<label for="name_item_menu">Name item menu</label>
						<input v-model="data_form.title"  type="text" placeholder="Input item menu name..." autofocus="autofocus" required="required" oninvalid="this.setCustomValidity('cannot be empty')" oninput="this.setCustomValidity('')" class="form-control">
					</div>
					<div class="form-group form-material">
						<label for="name_item_menu">Icon</label>
						<input v-model="data_form.icon"  type="text" placeholder="Input description icon menu..."  class="form-control"  >
					</div>

					<div class="form-group form-material">
						<label for="desc_item_menu">Url menu</label>
						<input v-model="data_form.url" type="text" placeholder="Input url menu..."  required="required" oninvalid="this.setCustomValidity('cannot be empty')" oninput="this.setCustomValidity('')" class="form-control">
					</div>
					<div class="form-group form-material">
						<button @click="add_menu" class="btn btn-primary" data-dismiss="modal" style="min-width: 100%">Add Menu</button>
					</div>
				</form>
			</div>
			<div class="offset-md-1 col-md-7 bg-white">
				<br>
				<div class="example-wrap m-md-0" style="background: rgb(228, 227, 227);padding: 15px;">
					<h5> List Menu :</h5>
						<div  class="menu_editor" data-plugin="nestable">
							<ol id="list-menu" class="dd-list" v-html="data_menu">
								<li  class="dd-item" data-id="1" data-title="item 1" data-url="" data-icon="">
									<div class="dd-handle">ITEM 1</div>
								</li>
							</ol>
						</div>

				</div>
				<div class="form-group mt-50">
					<button type="button" @click="set_data_menu" class="btn btn-primary" data-dismiss="modal" style="min-width: 100%">Save all change</button>
				</div>
			</div>

		</div>
	</div>
	<!-- End Panel Table Add Row -->
@endsection

@section('script-body')
	<script src="{{assets('jquery.nestable.min.js','vendor/remark/global/vendor/nestable')}}"></script>
<script>

		const config={
			headers: {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
				'Accept': 'Token',
				"Access-Control-Allow-Origin": "*",
			}
		};

		$(document).ready(function () {
			get_nestable();
			$('.menu_editor').on('change', function() {
				get_nestable();
			});
		});

		function get_nestable(){
			window.data_menu=JSON.stringify($('.menu_editor').nestable('serialize'))
		}

		$(document).on('shown.bs.modal', function (e) {
			$('[autofocus]', e.target).focus();

		});

		var app = new Vue({
			el: '#app',
			store,
			data: {
				base_url:'{{base_url()}}',
				data_menu:'',
				data_form:{},
				id_menu:0,
				name_hapus:0,
			},
			methods: {
				set_data_menu:function () {
					axios.post('{{base_url("cms/setting/update_data/").$breadcrumb[3]}}',$.param({'data_menu':data_menu}),config)
							.then(response => {
								this.data_menu = response.data.menu;
							})
							.catch(error=>{
								this.errors.push(error);
							});
				},

				add_menu:function () {
					this.data_menu=this.data_menu+
							'<li  class="dd-item" data-id="1" data-title="'+this.data_form.title+'" data-url="'+this.data_form.url+'" data-icon="'+this.data_form.icon+'">\n' +
							'<div class="dd-handle">'+this.data_form.title+'</div>\n' +
							'</li>'
				},

				delete_menu:function () {

					axios.get('{{base_url("cms/setting/delete_menu/")}}'+this.id_menu)
							.then( response => {
								if(response.data.status){
									toastr.success(response.data.message);
									$('#confirm-delete').modal('toggle');
									this.get_data_menu();
								}else{
									toastr.error(response.data.message);
								}
							})
							.catch(error => {
								this.errors.push(error)
							});
				}

			},
			mounted () {

			},created(){

			}
		});


	</script>
@endsection
