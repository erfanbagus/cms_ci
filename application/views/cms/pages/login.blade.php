@extends('cms.layouts.app_auth')

@section('content')
	<div class="page-content vertical-align-middle animation-slide-top animation-duration-1">
		<div class="panel">
			<div class="panel-body">
				<div class="brand">
					<img class="brand-img" src="{{assets('logo-colored.png','images')}}" alt="...">
					<h2 class="brand-text font-size-18">Remark</h2>
				</div>
				<form method="post" autocomplete="off" id="form_login">
					<div class="form-group form-material floating" data-plugin="formMaterial">
						<input type="text" class="form-control" name="i-username"  />
						<label class="floating-label">Username</label>
					</div>
					<div class="form-group form-material floating" data-plugin="formMaterial">
						<input type="password" class="form-control" name="i-password" />
						<label class="floating-label">Password</label>
					</div>
					<div class="form-group clearfix">
						<div class="checkbox-custom checkbox-inline checkbox-primary checkbox-lg float-left">
							<input type="checkbox" id="inputCheckbox" name="i-remember">
							<label for="inputCheckbox">Remember me</label>
						</div>
						<a class="float-right" href="forgot-password.html">Forgot password?</a>
					</div>
					<button type="submit" class="btn btn-primary btn-block btn-lg mt-40">Sign in</button>
					<input type="hidden" name="{{$csrf_token['name']}}" value="{{$csrf_token['value']}}">
				</form>

			</div>
		</div>

		<footer class="page-copyright page-copyright-inverse">
			<p>WEBSITE BY Creation Studio</p>
			<p>© 2018. All RIGHT RESERVED.</p>
			<div class="social">
				<a class="btn btn-icon btn-pure" href="javascript:void(0)">
					<i class="icon bd-twitter" aria-hidden="true"></i>
				</a>
				<a class="btn btn-icon btn-pure" href="javascript:void(0)">
					<i class="icon bd-facebook" aria-hidden="true"></i>
				</a>
				<a class="btn btn-icon btn-pure" href="javascript:void(0)">
					<i class="icon bd-google-plus" aria-hidden="true"></i>
				</a>
			</div>
		</footer>
	</div>
@endsection
@section('script')
	<script>

	</script>
@endsection
