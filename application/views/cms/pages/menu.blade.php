@extends('cms.layouts.app')

@section('head-body')
	<link rel="stylesheet" href="{{assets('dataTables.bootstrap4.css','vendor/remark/global/vendor/datatables.net-bs4')}}">
	<link rel="stylesheet" href="{{assets('dataTables.fixedheader.bootstrap4.css','vendor/remark/global/vendor/datatables.net-fixedheader-bs4')}}">
	<link rel="stylesheet" href="{{assets('dataTables.fixedcolumns.bootstrap4.css','vendor/remark/global/vendor/datatables.net-fixedcolumns-bs4')}}">
	<link rel="stylesheet" href="{{assets('dataTables.rowgroup.bootstrap4.css','vendor/remark/global/vendor/datatables.net-rowgroup-bs4')}}">
	<link rel="stylesheet" href="{{assets('dataTables.scroller.bootstrap4.css','vendor/remark/global/vendor/datatables.net-scroller-bs4')}}">
	<link rel="stylesheet" href="{{assets('dataTables.select.bootstrap4.css','vendor/remark/global/vendor/datatables.net-select-bs4')}}">
	<link rel="stylesheet" href="{{assets('dataTables.responsive.bootstrap4.css','vendor/remark/global/vendor/datatables.net-responsive-bs4')}}">
	<link rel="stylesheet" href="{{assets('dataTables.buttons.bootstrap4.css','vendor/remark/global/vendor/datatables.net-buttons-bs4')}}">
@endsection

@section('content')
	<!-- Modal -->
	<div class="modal fade" id="data_menu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Data Menu</h4>
				</div>
				<form method="post" id="input-data_menu"  onsubmit="return false" @submit.prevent="add_menu">
					<div class="modal-body">
						<div class="form-group">
							<input v-model="data_form.name" type="text" class="form-control" placeholder="Menu name" autofocus required  oninvalid="this.setCustomValidity('cannot be empty')" oninput="this.setCustomValidity('')">
						</div>
						<div class="form-group">
							<textarea v-model="data_form.description" type="text" class="form-control" placeholder="Menu description" style="height: 150px">
							</textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary">Save changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="border-bottom: 1px solid rgba(128,128,128,0.35)">
					Confirm Delete Data
				</div>

				<div class="modal-body">
					You are sure to delete this data [ @{{  name_hapus }} ]  ..?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<a @click="delete_menu()" class="btn btn-danger text-white " id="btn-ok">Delete</a>
				</div>
			</div>
		</div>
	</div>
	<!-- Panel Table Add Row -->
	<div class="panel" id="panel">

		<header class="panel-heading">
			<h3 class="panel-title">List Menu</h3>
		</header>
		<div class="panel-body table-responsive">
			<div class="row">
				<div class="col-md-2">
					<div class="mb-15">
						<button type="button" class="btn btn-block btn-primary waves-effect waves-classic" data-toggle="modal" data-target="#data_menu">
							<i class="icon md-plus-square" aria-hidden="true"></i>	<span style="position:relative;top:-5px">add menu</span>
						</button>
					</div>
				</div>
			</div>
			<table class="table table-hover table-striped" cellspacing="0" id="list_menu">
				<thead>
				<tr >
					<th class="text-center" width="10%">No</th>
					<th width="20%">Name Menu</th>
					<th width="40%">Description Menu</th>
					<th class="text-center" width="20%">Actions</th>
				</tr>
				</thead>
				<tbody>
				<tr v-for="menu in data_menu" >
					<td class="text-center">@{{ menu.id }}</td>
					<td>@{{ menu.name }}</td>
					<td>@{{ menu.description }}</td>
					<td class="actions text-center">
						<a :href="base_url+'cms/setting/edit_menu/'+menu.id" class="btn btn-sm btn-icon btn-pure btn-default on-default " >
							<i class="icon md-assignment text-success" aria-hidden="true" data-toggle="tooltip" data-original-title="Detail menu"></i>
						</a>
						<a @click="id_menu = menu.id;name_hapus=menu.name" href="javascript:void(0)" class="btn btn-sm btn-icon btn-pure btn-default on-default " data-toggle="modal" data-target="#confirm-delete">
							<i class="icon md-delete text-danger" aria-hidden="true" data-toggle="tooltip" data-original-title="Remove"></i>
						</a>
					</td>
				</tr>
				<tr v-if="data_menu.length <= 0">
					<td class="text-center" colspan="3">there is no data for now</td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- End Panel Table Add Row -->
@endsection

@section('script-body')
	<script src="{{assets('jquery.dataTables.js','vendor/remark/global/vendor/datatables.net')}}"></script>
	<script src="{{assets('dataTables.bootstrap4.js','vendor/remark/global/vendor/datatables.net-bs4')}}"></script>
	<script src="{{assets('dataTables.fixedHeader.js','vendor/remark/global/vendor/datatables.net-fixedheader')}}"></script>
	<script src="{{assets('dataTables.fixedColumns.js','vendor/remark/global/vendor/datatables.net-fixedcolumns')}}"></script>
	<script src="{{assets('dataTables.rowGroup.js','vendor/remark/global/vendor/datatables.net-rowgroup')}}"></script>
	<script src="{{assets('dataTables.scroller.js','vendor/remark/global/vendor/datatables.net-scroller')}}"></script>
	<script src="{{assets('dataTables.responsive.js','vendor/remark/global/vendor/datatables.net-responsive')}}"></script>
	<script src="{{assets('responsive.bootstrap4.js','vendor/remark/global/vendor/datatables.net-responsive-bs4')}}"></script>
	<script src="{{assets('dataTables.buttons.js','vendor/remark/global/vendor/datatables.net-buttons')}}"></script>
	<script src="{{assets('buttons.html5.js','vendor/remark/global/vendor/datatables.net-buttons')}}"></script>
	<script src="{{assets('buttons.flash.js','vendor/remark/global/vendor/datatables.net-buttons')}}"></script>
	<script src="{{assets('buttons.print.js','vendor/remark/global/vendor/datatables.net-buttons')}}"></script>
	<script src="{{assets('buttons.colVis.js','vendor/remark/global/vendor/datatables.net-buttons')}}"></script>
	<script src="{{assets('buttons.bootstrap4.js','vendor/remark/global/vendor/datatables.net-buttons-bs4')}}"></script>
	<script>
		const config={
			headers: {
				'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
				'Accept': 'Token',
				"Access-Control-Allow-Origin": "*",
			}
		}
		$(document).on('shown.bs.modal', function (e) {
			$('[autofocus]', e.target).focus();
		});



		var app = new Vue({
			el: '#app',
			store,
			data: {
				base_url:'{{base_url()}}',
				data_menu:'',
				data_form:{
				},
				id_menu:0,
				name_hapus:0,
			},
			methods: {
				get_data_menu:function () {

					axios.get('{{base_url("api/menu_data")}}')
							.then(response => {

									this.data_menu = response.data.menu;

							})
							.catch(error=>{
									this.errors.push(error);
							});
				},

				add_menu:function (event) {

					axios.post('{{base_url("cms/setting/add_menu/")}}',$.param(this.data_form),config)
							.then(response => {
								if(response.data.status){
									toastr.success(response.data.message);
								}else{
									toastr.error(response.data.message);
								}
								this.get_data_menu();
							})
							.catch(error=>{
								this.errors.push(error);
							});
				},

				delete_menu:function () {

					axios.get('{{base_url("cms/setting/delete_menu/")}}'+this.id_menu)
							.then( response => {
								if(response.data.status){
									toastr.success(response.data.message);
									$('#confirm-delete').modal('toggle');
									this.get_data_menu();
								}else{
									toastr.error(response.data.message);
								}
							})
							.catch(error => {
								this.errors.push(error)
							});
				}

			},
			mounted () {
				this.get_data_menu();
			},created(){

			}
		});


	</script>
@endsection
