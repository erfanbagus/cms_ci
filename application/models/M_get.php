<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_get extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function get_all_data($table,$select=null,$limit=null,$start=null){
		$this->db->select($select);
		$this->db->limit($limit,$start);
		return $this->db->get($table);

	}

	public function add_menu($data){
		 return $this->db->insert('menu',$data);
	}

	public function delete_menu($id){
		$this->db->where('id',$id);
		$this->db->delete('menu');
		if ($this->db->affected_rows() > 0) {
			return TRUE;
		}
		return FALSE;
	}
}
