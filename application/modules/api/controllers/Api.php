<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CC_Rest {

	function __construct()
	{
		parent::__construct();
		$this->load->library(array('ion_auth'));
		$this->load->helper(array('language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');

	}

	public function cms_data_get()
	{
		$data=array();
		$data['profile']=$this->ion_auth->user()->row();
		$this->response($data,200);
	}

	public function menu_data_get($num_data = 0,$data_on_page=10)
	{
		$data=array();
		$table=$this->m_get->get_all_data('menu','id,name,description',null,$num_data);
		$data['data_start']=$num_data;
		$data['data_per_page']=$data_on_page;
		$data['data_total']=$table->num_rows();
		$data['menu']= $table->result();
		$this->response($data,200);
	}
}
