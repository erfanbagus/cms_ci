<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front extends CC_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function index()
	{
		$data=$this->getdata();
		$this->blade_render->views('front.pages.welcome',$data);
	}
}
