@extends('layouts.app')

@section('content')
	<div class="page-header">
		<h1 class="page-title font-size-26 font-weight-100">Details Of Web Information</h1>
	</div>

	<div class="page-content container-fluid">
		<div class="row">
			<!-- First Row -->
			<div class="col-xl-3 col-md-6 info-panel">
				<div class="card card-shadow">
					<div class="card-block bg-white p-20">
						<button type="button" class="btn btn-floating btn-sm btn-success">
							<i class="icon wb-shopping-cart"></i>
						</button>
						<span class="ml-15 font-weight-400">TOTAL Arti</span>
						<div class="content-text text-center mb-0">
							<span class="font-size-40 font-weight-100">1680</span>
							<p class="blue-grey-400 font-weight-100 m-0">buku per bulan ini</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-md-6 info-panel">
				<div class="card card-shadow">
					<div class="card-block bg-white p-20">
						<button type="button" class="btn btn-floating btn-sm btn-danger">
							<i class="icon wb-book"></i>
						</button>
						<span class="ml-15 font-weight-400">BUKU MASUK</span>
						<div class="content-text text-center mb-0">
							<span class="font-size-40 font-weight-100">2000</span>
							<p class="blue-grey-400 font-weight-100 m-0">buku per bulan ini</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-md-6 info-panel">
				<div class="card card-shadow">
					<div class="card-block bg-white p-20">
						<button type="button" class="btn btn-floating btn-sm btn-warning">
							<i class="icon wb-tag"></i>
						</button>
						<span class="ml-15 font-weight-400">FAKTUR</span>
						<div class="content-text text-center mb-0">
							<span class="font-size-40 font-weight-100">320</span>
							<p class="blue-grey-400 font-weight-100 m-0">buku per bulan ini</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-md-6 info-panel">
				<div class="card card-shadow">
					<div class="card-block bg-white p-20">
						<button type="button" class="btn btn-floating btn-sm btn-dark">
							<i class="icon wb-users"></i>
						</button>
						<span class="ml-15 font-weight-400">RELASI</span>
						<div class="content-text text-center mb-0">
							<span class="font-size-40 font-weight-100">114</span>
							<p class="blue-grey-400 font-weight-100 m-0">Toko</p>
						</div>
					</div>
				</div>
			</div>
			<!-- End First Row -->

			<!-- second Row -->
			<div class="col-lg-12" id="ecommerceRecentOrder">
				<div class="card card-shadow table-row">
					<div class="card-header card-header-transparent py-20">
						<div class="btn-group dropdown">
							<a href="#" class="text-body dropdown-toggle blue-grey-700" data-toggle="dropdown">Transaksi Terbaru</a>
							<div class="dropdown-menu animate" role="menu">
								<a class="dropdown-item" href="#" role="menuitem">Toko</a>
								<a class="dropdown-item" href="#" role="menuitem">Total Buku</a>
								<a class="dropdown-item" href="#" role="menuitem">Tanggal</a>
							</div>
						</div>
					</div>
					<div class="card-block bg-white table-responsive">
						<table class="table">
							<thead>
							<tr>
								<th>Gambar</th>
								<th>Produk</th>
								<th>Nama Toko</th>
								<th>Tanggal</th>
								<th>Jumlah</th>
								<th>Status</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>
									<img src="{{assets('imac.png','vendor/remark/assets/examples/images/products')}}"
										 alt="iMac" />
								</td>
								<td>THE KING MASTER IPA SMP</td>
								<td>TB.GRAMEDIA Puri Indah 10134</td>
								<td>22/08/2018</td>
								<td>516</td>
								<td>
									<span class="badge badge-success font-weight-100">Selesai</span>
								</td>
							</tr>
							<tr>
								<td>
									<img src="{{assets('iphone.png','vendor/remark/assets/examples/images/products')}}"
										 alt="iPhone" />
								</td>
								<td>FOKUS BEDAH KISI – KISI CAT CPNS</td>
								<td>TB.GRAMEDIA Jbk Margonda Depok 10150</td>
								<td>15/07/2018</td>
								<td>480</td>
								<td>
									<span class="badge badge-warning font-weight-100">Proses</span>
								</td>
							</tr>
							<tr>
								<td>
									<img src="{{assets('applewatch.png','vendor/remark/assets/examples/images/products')}}"
										 alt="apple_watch" />
								</td>
								<td>THE KING MASTER MATEMATIKA SMA</td>
								<td>TB.GRAMEDIA Yogya Sudirman 10105</td>
								<td>10/07/2018</td>
								<td>800</td>
								<td>
									<span class="badge badge-success font-weight-100">Selesai</span>
								</td>
							</tr>
							<tr>
								<td>
									<img src="{{assets('macmouse.png','vendor/remark/assets/examples/images/products')}}"
										 alt="mac_mouse" />
								</td>
								<td>THE KING MASTER FISIKA SMA</td>
								<td>TB.GRAMEDIA Sby Expo 10183</td>
								<td>22/04/2018</td>
								<td>375</td>
								<td>
									<span class="badge badge-default font-weight-100">Gagal</span>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- End Second Row -->
		</div>
	</div>
@endsection
