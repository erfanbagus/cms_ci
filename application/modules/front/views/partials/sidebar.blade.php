<div class="site-menubar">
	<div class="site-menubar-body">
		<div>
			<div>
				<ul class="site-menu" data-plugin="menu">
					<li class="site-menu-category">MENU</li>
					<li class="site-menu-item active">
						<a class="animsition-link" href="index.html">
							<i class="site-menu-icon wb-grid-4" aria-hidden="true"></i>
							<span class="site-menu-title">Dashboard</span>
						</a>
					</li>
					<li class="site-menu-item has-sub">
						<a href="javascript:void(0)">
							<i class="site-menu-icon wb-briefcase" aria-hidden="true"></i>
							<span class="site-menu-title">Settings</span>
							<span class="site-menu-arrow"></span>
						</a>
						<ul class="site-menu-sub">
							<li class="site-menu-item">
								<a class="animsition-link" href="kategori_buku.html">
									<span class="site-menu-title">Kategori Buku</span>
								</a>
							</li>
							<li class="site-menu-item">
								<a class="animsition-link" href="data_diskon.html">
									<span class="site-menu-title">Data Diskon</span>
								</a>
							</li>
							<li class="site-menu-item">
								<a class="animsition-link" href="data_buku.html">
									<span class="site-menu-title">Data Buku</span>
								</a>
							</li>
							<li class="site-menu-item">
								<a class="animsition-link" href="data_pemasok.html">
									<span class="site-menu-title">Data Pemasok</span>
								</a>
							</li>
							<li class="site-menu-item">
								<a class="animsition-link" href="data_relasi.html">
									<span class="site-menu-title">Data Relasi</span>
								</a>
							</li>
						</ul>
					</li>

				</ul>
			</div>
		</div>
	</div>

</div>
