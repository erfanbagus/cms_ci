<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cms_setting extends CC_Cms {

	public function __construct() {

		parent::__construct();
		$this->load->library('migration');
	}

	public function index() {
		echo "test";
	}

	public function menu() {
		$this->C_cek_login(null,'cms/login');
		$data=$this->getdata();
		$data['title']= 'Menu';
		$this->blade_render->views('cms.pages.menu',$data);
	}

	public  function  add_menu(){
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$data=$this->input->post();
			$add = $this->m_get->add_menu($data);
			if($add){
				$data_json=array(
					"status"=>true,
					"message"=>"Berhasil menambah data"
				);
			}else{
				$data_json=array(
					"status"=>false,
					"message"=>"Gagal menambah data"
				);
			}
			header('Content-Type: application/json');
			echo json_encode($data_json);
			exit();
		}
		show_404();
	}

	public  function  edit_menu(){
		$this->C_cek_login(null,'cms/login');
		$data=$this->getdata();
		$data['title']= 'Menu | Edit';
		$this->blade_render->views('cms.pages.menu_edit',$data);
	}

	public function update_data(){
		$this->arc_nestable->set_json($this->input->post("data_menu"));
		$object=$this->arc_nestable->get_object();
		my_test($this->arc_nestable->get_generated_menu((object)$object));
	}

	public  function  delete_menu(){
		$id=(int)$this->uri->segment(4);
		$delete= $this->m_get->delete_menu($id);
		if($delete){
			$data_json=array(
				"status"=>true,
				"message"=>"Berhasil menghapus data"
			);
		}else{
			$data_json=array(
				"status"=>false,
				"message"=>"Gagal menghapus data"
			);
		}
		header('Content-Type: application/json');
		echo json_encode($data_json);
		exit();
	}

}
