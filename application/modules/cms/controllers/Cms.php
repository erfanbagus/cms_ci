<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cms extends CC_Cms {

	public function __construct() {

		parent::__construct();

	}

	public function index()
	{
		$this->C_cek_login(null,'cms/login');
		$data=$this->getdata();
		$data['title']= 'Dashboard';
		$this->blade_render->views('cms.pages.dashboard',$data);
	}


	//	--------- auth --------------------
	public function login()
	{
		$this->C_cek_login('cms','');

		if ($this->input->server('REQUEST_METHOD') == 'POST'){
//			my_test($this->input->post());
			$username = $this->input->post('i-username');
			$password = $this->input->post('i-password');
			$remember = (bool) $this->input->post('i-remember');

			if($this->C_login($username,$password,$remember)){
				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('cms', 'refresh');
			}
			else
			{
				// if the login was un-successful
				// redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('cms/login', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
			}
		}
		$data=$this->getdata();
		$data['title_apps']=$data['title_apps'].' | Login';
		$this->blade_render->views('cms.pages.login',$data);


	}

	public  function  logout(){
		$this->C_logout('cms/login');
	}
	//	--------- auth --------------------
}
