<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cms_migration extends CC_Controller {

	public function __construct() {

		parent::__construct();
		$this->load->library('migration');

	}

	public function index() {
		if (!$this->migration->current()) {
			show_error($this->migration->error_string());
		} else {
			echo 'migration worked!';
		}
	}

}
